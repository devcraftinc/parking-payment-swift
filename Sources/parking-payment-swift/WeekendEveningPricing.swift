﻿// Copyright 2018 DevCraft Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import Foundation

class WeekendEveningPricing : Pricing {
  let underlying : Pricing
  let dayOfWeek : DayOfWeek
  let eveningThreshold : TimeSpan
  let weekendEveningPrice : Int

  init(underlying : Pricing, dayOfWeek : DayOfWeek, eveningThreshold : TimeSpan, weekendEveningPrice : Int) {
    self.underlying = underlying
    self.dayOfWeek = dayOfWeek
    self.eveningThreshold = eveningThreshold
    self.weekendEveningPrice = weekendEveningPrice
  }

  public func calculatePrice(for : Interval) -> Int {
    var  midpoint = Calendar.current.startOfDay(for: `for`.start)
    while (getDayOfWeek(from: midpoint) != dayOfWeek) {
      midpoint = addDays(value: -1, to: midpoint)
    }

    midpoint = changeTimeOfDay(from: midpoint, to: eveningThreshold)

    let night = `for`.startingAt(midpoint)
    let day = `for`.endBefore(midpoint)

    var price = 0;

    if day.exists {
      price += underlying.calculatePrice(for: day);
    }

    if night.exists {
      price += weekendEveningPrice;
    }

    return price;
  }
}