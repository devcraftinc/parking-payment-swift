﻿// Copyright 2018 DevCraft Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

public class PricingBuilder
{
  public var weekendDays : [DayOfWeek] = []
  
  public func configure(initialBlockLength : TimeSpan) {
  }

  public func configure(initialBlockPrice : Int) {
  }

  public func configure(subsequentIncrementLength : TimeSpan) {
  }

  public func configure(subsequentIncrementPrice : Int) {
  }

  public func configure(morningStartsAt : TimeSpan) {
  }

  public func configure(weekendEveningPrice : Int) {
  }

  public func configure(weekendEveningStartsAt : TimeSpan) {
  }

  public func configure(dailyMaximumAppliesToWeekendEvenings : Bool) {
  }

  public func configure(dailyMaximum: Int) {
  }

  public init() {
  }

  public func build() -> Pricing {
    return NullPricing()
  }

  private class NullPricing : Pricing {
    public func calculatePrice(for : Interval) -> Int {
      return 0
    }
  }
}