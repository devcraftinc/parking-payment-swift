// Copyright 2018 DevCraft Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import Foundation

public struct TimeSpan {
    private static let secondsInMinute : Int = 60
    private static let minutesInHour : Int = 60
    private static let hoursInDay : Int = 24
    private static let nanosecondsInSecond : Int = 1000000000

    public let Ticks : TimeInterval

    public static let Zero = TimeSpan(0)

    public init(_ ticks : TimeInterval) {
        self.Ticks = ticks
    }

    public static func FromDays(_ days : Double) -> TimeSpan {
        return FromHours(days * Double(TimeSpan.hoursInDay))
    }
    
    public static func FromHours(_ hours : Double) -> TimeSpan {
        return FromMinutes(hours * Double(TimeSpan.minutesInHour))
    }

    public static func FromMinutes(_ minutes: Double) -> TimeSpan {
        return TimeSpan(minutes * Double(TimeSpan.secondsInMinute))
    }

    public static func FromTicks(_ ticks : Double) -> TimeSpan {
        return TimeSpan(ticks)
    }

    public var days : Int {
        get {
            return Int(Ticks / Double(TimeSpan.hoursInDay * TimeSpan.minutesInHour * TimeSpan.secondsInMinute))
        }
    }

    public var hours : Int {
        get {
            return Int(Ticks / Double(TimeSpan.minutesInHour * TimeSpan.secondsInMinute)) % TimeSpan.hoursInDay
        }
    }

    public var minutes : Int {
        get {
            return Int(Ticks / Double(TimeSpan.secondsInMinute)) % TimeSpan.minutesInHour
        }
    }

    public var seconds : Int {
        get {
            return Int(Ticks) % TimeSpan.secondsInMinute
        }
    }

    public var nanoseconds : Int {
        get {
            return Int(Ticks.truncatingRemainder(dividingBy: 1) * Double(TimeSpan.nanosecondsInSecond))
        }
    }
}

public func +(left: TimeSpan, right:TimeSpan) -> TimeSpan {
    return TimeSpan(left.Ticks + right.Ticks)
}

public func -(left: TimeSpan, right: TimeSpan) -> TimeSpan {
    return TimeSpan(left.Ticks - right.Ticks)
}

public func <(left: TimeSpan, right: TimeSpan) -> Bool {
    return left.Ticks < right.Ticks
}

public func *(left: TimeSpan, right: Double) -> TimeSpan {
    return TimeSpan(left.Ticks * right)
}

public func *(left: TimeSpan, right: Int) -> TimeSpan {
    return TimeSpan(left.Ticks * Double(right))
}

public func +(left: Date, right: TimeSpan) -> Date {
    return left + right.Ticks
}