﻿// Copyright 2018 DevCraft Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import Foundation

class DayOfWeekPricing : Pricing {
  let perDay : [DayOfWeek:Pricing]
  let morningThreshold : TimeSpan

  init(perDay : [DayOfWeek:Pricing], morningThreshold : TimeSpan) {
    self.perDay = perDay
    self.morningThreshold = morningThreshold
  }

  public func calculatePrice(for : Interval) -> Int {
    var currentInterval = `for`
    var morning = changeTimeOfDay(from: currentInterval.start, to:morningThreshold)

    if morning > currentInterval.start {
      morning = addDays(value: -1, to: morning)
    }

    var price = 0

    repeat
    {
      let pricing = perDay[getDayOfWeek(from: morning)]!
      morning = addDays(value: 1, to: morning)
      let timeToday = currentInterval.endBefore(morning)
      price += pricing.calculatePrice(for: timeToday)
      currentInterval = currentInterval.startingAt(morning)
    } while currentInterval.exists

    return price
  }
}