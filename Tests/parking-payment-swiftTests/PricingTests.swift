﻿// Copyright 2018 DevCraft Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import Foundation
import XCTest
import parking_payment_swift

public class PricingTests : XCTestCase {
  let defaultEveningThreshold : TimeSpan = TimeSpan.FromHours(22)
  let defaultInitialCost : Int = 10
  let defaultInitialBlockLength : TimeSpan = TimeSpan.FromHours(1)
  let defaultGracePeriod : TimeSpan = TimeSpan.FromMinutes(10)
  let defaultIncrementLength : TimeSpan = TimeSpan.FromMinutes(15)
  let defaultIncrementCost : Int = 3
  let defaultWeekendEveningPrice : Int = 40
  let defaultMaximumDailyCost : Int = 80
  let defaultMorningThreshold : TimeSpan = TimeSpan.FromHours(6)

  var actualPrice : Int = 0
  var entryTime : Date = Date()
  var paymentTime : Date = Date()
  var pricingBuilder : PricingBuilder = PricingBuilder()

  public override func setUp() {
    pricingBuilder = PricingBuilder()
  }

  static var allTests = [
    ("gracePeriod", testGracePeriod),
    ("testFirstHour", testFirstHour),
    ("testFirstIncrement", testFirstIncrement),
    ("testNthIncrement", testNthIncrement),
    ("testMaximumDailyPrice", testMaximumDailyPrice),
    ("testWeekendEvening", testWeekendEvening),
    ("testWeekdayEvening", testWeekdayEvening),
    ("testWeekendDay", testWeekendDay),
    ("testStraddleWeekendDayAndNight", testStraddleWeekendDayAndNight),
    ("testLongStretchIntoWeekendNightStartingBeforeEvening", testLongStretchIntoWeekendNightStartingBeforeEvening),
    ("testLateNightWeekendOutBeforeNewDayStarts", testLateNightWeekendOutBeforeNewDayStarts),
    ("testLongInitialSegment", testLongInitialSegment),
    ("testChangeMorning", testChangeWeekend),
    ("testChangeInitialBlockPrice", testChangeInitialBlockPrice),
    ("testChangeIncrementLength", testChangeIncrementLength),
    ("testChangeIncrementCost", testChangeIncrementCost),
    ("testChangeWeekendEveningPrice", testChangeWeekendEveningPrice),
    ("testChangeWeekendEveningThreshold", testChangeWeekendEveningThreshold),
    ("testChangeWeekendEveningThresholdToMidnight", testChangeWeekendEveningThresholdToMidnight),
    ("testChangeDailyMaximum", testChangeDailyMaximum),
    ("testMakeDailyMaximumExcludeWeekendEvenings", testMakeDailyMaximumExcludeWeekendEvenings),
    ("testDailyMaximumAutomaticallyIncludesWeekendEvenings", testDailyMaximumAutomaticallyIncludesWeekendEvenings),
  ]

  public func testGracePeriod() {
    timeInParkingShouldCost(timeInParking: justBefore(defaultGracePeriod), expectedPrice: 0)
  }

  public func testFirstHour() {
    timeInParkingShouldCost(timeInParking: defaultGracePeriod, expectedPrice: defaultInitialCost)
    timeInParkingShouldCost(timeInParking: justBefore(defaultInitialBlockLength), expectedPrice: defaultInitialCost)
  }

  public func testFirstIncrement() {
    timeInParkingShouldCost(timeInParking: defaultInitialBlockLength, expectedPrice: defaultInitialCost + defaultIncrementCost)
    timeInParkingShouldCost(timeInParking: defaultInitialBlockLength + justBefore(defaultIncrementLength), expectedPrice: defaultInitialCost + defaultIncrementCost)
  }

  public func testNthIncrement() {
    timeInParkingShouldCost(timeInParking: defaultInitialBlockLength + defaultIncrementLength * defaultIncrementCost, expectedPrice: defaultInitialCost + 4 * defaultIncrementCost)
    timeInParkingShouldCost(timeInParking: defaultInitialBlockLength + justBefore(defaultIncrementLength * 4), expectedPrice: defaultInitialCost + 4 * defaultIncrementCost)
  }

  public func testMaximumDailyPrice() {
    timeInParkingShouldCost(timeInParking: defaultInitialBlockLength + defaultIncrementLength * 45, expectedPrice: defaultMaximumDailyCost)
    timeInParkingShouldCost(timeInParking: defaultInitialBlockLength + TimeSpan.FromDays(1.5), expectedPrice: defaultMaximumDailyCost * 2)
    timeInParkingShouldCost(timeInParking: defaultInitialBlockLength + TimeSpan.FromDays(2.5), expectedPrice: defaultMaximumDailyCost * 3)
  }

  public func testWeekendEvening() {
    dayOfWeekAndTimeOfDayPrice(dayOfWeek: .friday, entryTime: defaultEveningThreshold, timeInParking: defaultGracePeriod, expectedPrice: defaultWeekendEveningPrice)
    dayOfWeekAndTimeOfDayPrice(dayOfWeek: .saturday, entryTime: defaultEveningThreshold, timeInParking: defaultGracePeriod, expectedPrice: defaultWeekendEveningPrice)
  }

  public func testWeekdayEvening() {
    dayOfWeekAndTimeOfDayPrice(dayOfWeek: .sunday, entryTime: defaultEveningThreshold, timeInParking: defaultGracePeriod, expectedPrice: defaultInitialCost)
    dayOfWeekAndTimeOfDayPrice(dayOfWeek: .monday, entryTime: defaultEveningThreshold, timeInParking: defaultGracePeriod, expectedPrice: defaultInitialCost)
    dayOfWeekAndTimeOfDayPrice(dayOfWeek: .tuesday, entryTime: defaultEveningThreshold, timeInParking: defaultGracePeriod, expectedPrice: defaultInitialCost)
    dayOfWeekAndTimeOfDayPrice(dayOfWeek: .wednesday, entryTime: defaultEveningThreshold, timeInParking: defaultGracePeriod, expectedPrice: defaultInitialCost)
    dayOfWeekAndTimeOfDayPrice(dayOfWeek: .thursday, entryTime: defaultEveningThreshold, timeInParking: defaultGracePeriod, expectedPrice: defaultInitialCost)
  }

  public func testWeekendDay() {
    dayOfWeekAndTimeOfDayPrice(dayOfWeek: .friday, entryTime: justBefore(defaultEveningThreshold - defaultGracePeriod), timeInParking: defaultGracePeriod, expectedPrice: defaultInitialCost)
  }

  public func testStraddleWeekendDayAndNight() {
    dayOfWeekAndTimeOfDayPrice(dayOfWeek: .friday, entryTime: justBefore(defaultEveningThreshold), timeInParking: defaultGracePeriod, expectedPrice: defaultInitialCost + defaultWeekendEveningPrice)
  }

  public func testLongStretchIntoWeekendNightStartingBeforeEvening() {
    givenEntryTime(.friday, justBefore(defaultEveningThreshold))
    givenPaymentTime(entryTime + TimeSpan.FromHours(4))

    whenPriceCalculated()

    thenPriceIs(defaultInitialCost + defaultWeekendEveningPrice)
  }

  public func testLateNightWeekendOutBeforeNewDayStarts() {
    givenEntryTime(.friday, defaultEveningThreshold)
    givenPaymentTime(entryTime + justBefore(TimeSpan.FromHours(8)))

    whenPriceCalculated()

    thenPriceIs(defaultWeekendEveningPrice)
  }

  public func testLongInitialSegment() {
    let span = TimeSpan.FromHours(3)
    pricingBuilder.configure(initialBlockLength: span)

    timeInParkingShouldCost(timeInParking: justBefore(span), expectedPrice: defaultInitialCost)
  }

  public func testChangeMorning() {
    let morning = TimeSpan.FromHours(5)
    pricingBuilder.configure(morningStartsAt: morning)

    crossMorningThresholdPricing(dayOfWeek: .monday, morning: morning, day1Price: defaultInitialCost, day2Price: defaultInitialCost)
    crossMorningThresholdPricing(dayOfWeek: .tuesday, morning: morning, day1Price: defaultInitialCost, day2Price: defaultInitialCost)
    crossMorningThresholdPricing(dayOfWeek: .wednesday, morning: morning, day1Price: defaultInitialCost, day2Price: defaultInitialCost)
    crossMorningThresholdPricing(dayOfWeek: .thursday, morning: morning, day1Price: defaultInitialCost, day2Price: defaultInitialCost)
    crossMorningThresholdPricing(dayOfWeek: .friday, morning: morning, day1Price: defaultInitialCost, day2Price: defaultInitialCost)
    crossMorningThresholdPricing(dayOfWeek: .saturday, morning: morning, day1Price: defaultWeekendEveningPrice, day2Price: defaultInitialCost)
    crossMorningThresholdPricing(dayOfWeek: .sunday, morning: morning, day1Price: defaultWeekendEveningPrice, day2Price: defaultInitialCost)
  }

  public func testChangeWeekend() {
    pricingBuilder.weekendDays += [.tuesday]

    crossMorningThresholdPricing(dayOfWeek: .wednesday, morning: defaultMorningThreshold, day1Price: defaultWeekendEveningPrice, day2Price: defaultInitialCost)
  }

  public func testChangeInitialBlockPrice() {
    let expectedPrice : Int = 25
    pricingBuilder.configure(initialBlockPrice: expectedPrice)

    timeInParkingShouldCost(timeInParking: defaultGracePeriod, expectedPrice: expectedPrice)
  }

  public func testChangeIncrementLength() {
    let length = TimeSpan.FromHours(1)
    pricingBuilder.configure(subsequentIncrementLength: length)

    timeInParkingShouldCost(timeInParking: defaultInitialBlockLength, expectedPrice: defaultInitialCost + defaultIncrementCost)
    timeInParkingShouldCost(timeInParking: defaultInitialBlockLength + justBefore(length), expectedPrice: defaultInitialCost + defaultIncrementCost)
  }

  public func testChangeIncrementCost() {
    let price = 7
    pricingBuilder.configure(subsequentIncrementPrice: price)

    timeInParkingShouldCost(timeInParking: defaultInitialBlockLength, expectedPrice: defaultInitialCost + price)
    timeInParkingShouldCost(timeInParking: defaultInitialBlockLength + justBefore(defaultIncrementLength), expectedPrice: defaultInitialCost + price)
  }

  public func testChangeWeekendEveningPrice() {
    let price : Int = 79
    pricingBuilder.configure(weekendEveningPrice: price)

    dayOfWeekAndTimeOfDayPrice(dayOfWeek: .friday, entryTime: defaultEveningThreshold, timeInParking: defaultGracePeriod, expectedPrice: price)
  }

  public func testChangeWeekendEveningThreshold() {
    let threshold = TimeSpan.FromHours(20)
    pricingBuilder.configure(weekendEveningStartsAt: threshold)

    dayOfWeekAndTimeOfDayPrice(dayOfWeek: .saturday, entryTime: threshold, timeInParking: defaultGracePeriod, expectedPrice: defaultWeekendEveningPrice)
  }

  public func testChangeWeekendEveningThresholdToMidnight() {
    pricingBuilder.configure(weekendEveningStartsAt: TimeSpan.FromHours(24))

    dayOfWeekAndTimeOfDayPrice(dayOfWeek: .friday, entryTime: TimeSpan.Zero, timeInParking: defaultGracePeriod, expectedPrice: defaultInitialCost)
    dayOfWeekAndTimeOfDayPrice(dayOfWeek: .saturday, entryTime: TimeSpan.Zero, timeInParking: defaultGracePeriod, expectedPrice: defaultWeekendEveningPrice)
    dayOfWeekAndTimeOfDayPrice(dayOfWeek: .sunday, entryTime: justBefore(TimeSpan.Zero), timeInParking: defaultGracePeriod, expectedPrice: defaultInitialCost + defaultWeekendEveningPrice)
    dayOfWeekAndTimeOfDayPrice(dayOfWeek: .sunday, entryTime: TimeSpan.Zero, timeInParking: defaultGracePeriod, expectedPrice: defaultWeekendEveningPrice)
    dayOfWeekAndTimeOfDayPrice(dayOfWeek: .monday, entryTime: TimeSpan.Zero, timeInParking: defaultGracePeriod, expectedPrice: defaultInitialCost)
  }

  public func testChangeDailyMaximum() {
    let limit : Int = 63
    pricingBuilder.configure(dailyMaximum: limit)

    timeInParkingShouldCost(timeInParking: TimeSpan.FromHours(12), expectedPrice: limit)
  }

  public func testMakeDailyMaximumExcludeWeekendEvenings() {
    pricingBuilder.configure(dailyMaximumAppliesToWeekendEvenings: false)

    dayOfWeekAndTimeOfDayPrice(dayOfWeek: .friday, entryTime: TimeSpan.FromHours(12), timeInParking: TimeSpan.FromHours(11), expectedPrice: defaultMaximumDailyCost + defaultWeekendEveningPrice)
  }

  public func testDailyMaximumAutomaticallyIncludesWeekendEvenings() {
    dayOfWeekAndTimeOfDayPrice(dayOfWeek: .friday, entryTime: TimeSpan.FromHours(12), timeInParking: TimeSpan.FromHours(11), expectedPrice: defaultMaximumDailyCost)
  }

  private func crossMorningThresholdPricing(dayOfWeek : DayOfWeek, morning : TimeSpan, day1Price : Int, day2Price : Int) {
    dayOfWeekAndTimeOfDayPrice(dayOfWeek: dayOfWeek, entryTime: justBefore(morning), timeInParking: defaultGracePeriod, expectedPrice: day1Price + day2Price)
  }

  private func dayOfWeekAndTimeOfDayPrice(dayOfWeek : DayOfWeek, entryTime : TimeSpan, timeInParking : TimeSpan, expectedPrice : Int) {
    givenEntryTime(dayOfWeek, entryTime)
    givenPaymentTime(self.entryTime + timeInParking)

    whenPriceCalculated()

    thenPriceIs(expectedPrice)
  }

  private func timeInParkingShouldCost(timeInParking: TimeSpan, expectedPrice: Int) {
    givenEntryTime()
    givenPaymentTime(entryTime + timeInParking)

    whenPriceCalculated()

    thenPriceIs(expectedPrice)
  }

  private func givenEntryTime()  {
    givenEntryTime(.monday, TimeSpan.FromHours(7))
  }

  private func givenEntryTime(_ dayOfWeek : DayOfWeek, _ offset : TimeSpan) {
    var dateTime = Calendar.current.startOfDay(for:Date())
    while (getDayOfWeek(from:dateTime) != dayOfWeek) {
      dateTime = addDays(value: 1, to: dateTime)
    }
    entryTime = changeTimeOfDay(from: dateTime, to:offset)
  }

  private func givenPaymentTime(_ paymentTime : Date) {
    self.paymentTime = paymentTime
  }

  private func whenPriceCalculated() {
    actualPrice = pricingBuilder.build().calculatePrice(for: Interval(start: entryTime, end: paymentTime))
  }

  private func thenPriceIs(_ expectedPrice: Int) {
    XCTAssertEqual(actualPrice, expectedPrice)
  }

  private func justBefore(_ ts: TimeSpan) -> TimeSpan {
    return ts - TimeSpan.FromTicks(1)
  }
}
